﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
    internal class Soal01
    {
        public Soal01()
        {
            Console.WriteLine("---Soal 01---");
            Console.WriteLine("Masukkan Tanggal Peminjaman (yyyy-mm-dd)   : ");
            string [] inputPinjam = Console.ReadLine().Split('-');
            Console.WriteLine("Masukkan Tanggal Pengembalian (yyyy-mm-dd) : ");
            string[] inputKembali = Console.ReadLine().Split('-');


            DateTime pinjam = new DateTime(int.Parse(inputPinjam[0]), int.Parse(inputPinjam[1]), int.Parse(inputPinjam[2]));
            DateTime kembali = new DateTime(int.Parse(inputKembali[0]), int.Parse(inputKembali[1]), int.Parse(inputKembali[2]));
            NotaPengembalian(kembali, pinjam);
        }
        public static void NotaPengembalian(DateTime kembali, DateTime pinjam)
        {
            TimeSpan intervalPinjam = kembali - pinjam;
            int totalDenda = 0, math = 750 * (intervalPinjam.Days - 4), kalkulus = 1000 * (intervalPinjam.Days - 5), strukturData = 1500 * (intervalPinjam.Days - 7);
            if (intervalPinjam.Days < 7 && intervalPinjam.Days > 4)
            {
                if (intervalPinjam.Days < 5)
                    totalDenda = math;
                else
                    totalDenda = math + kalkulus;
            }
            else
                totalDenda = math + kalkulus + strukturData;

            Console.WriteLine($"Dikarenakan keterlambatan pengembalian buku selama {intervalPinjam.Days} hari ");
            Console.WriteLine($"Maka anda harus membayarkan denda sebesar : {totalDenda} ");
        }
    }
}


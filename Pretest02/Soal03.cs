﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
    public class Soal03
    {
        public Soal03()
        {
            Console.WriteLine("Waktu masuk (yyyy-MM-ddThh:mm:ss) : ");
            string[] dtMasuk = Console.ReadLine().Split('T');
            string[] dateMasuk = dtMasuk[0].Split('-');
            string[] timeMasuk = dtMasuk[1].Split(':');
            DateTime masuk = new DateTime(int.Parse(dateMasuk[0]), int.Parse(dateMasuk[1]), int.Parse(dateMasuk[2]), int.Parse(timeMasuk[0]), int.Parse(timeMasuk[1]), int.Parse(timeMasuk[2]));

            Console.WriteLine("Waktu keluar (yyyy-MM-ddThh:mm:ss) : ");
            string[] dtKeluar = Console.ReadLine().Split('T');
            string[] dateKeluar = dtKeluar[0].Split('-');
            string[] timeKeluar = dtKeluar[1].Split(':');
            DateTime keluar = new DateTime(int.Parse(dateKeluar[0]), int.Parse(dateKeluar[1]), int.Parse(dateKeluar[2]), int.Parse(timeKeluar[0]), int.Parse(timeKeluar[1]), int.Parse(timeKeluar[2]));

            Console.WriteLine(NotaParkir(masuk, keluar));
        }
        public static string NotaParkir(DateTime masuk, DateTime keluar)
        {
            TimeSpan intervalParkir = keluar - masuk;
            int hours = Convert.ToInt16(Math.Ceiling(intervalParkir.TotalHours));
            int days = hours / 24;
            hours = hours - (days - 24);
            double tarif = days * 20000;
            for(int i = 1; i <= hours; i++)
            {
                if (i == 1)
                    tarif += 5000;

            }
            return $"{totalTarif}";
        }
    }
}

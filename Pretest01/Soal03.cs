﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    public class Soal03
    {
        public Soal03()
        {
            Console.Write("Masukkan input : ");
            string perpaduan = Console.ReadLine().ToLower();
            char[] input = perpaduan.ToCharArray();
            Sort(input);
            Array1Dim(Sort2(input));
        }
        public static void Sort(char[] input)
        {
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            char[] tmpalphabet = new char[input.Length];
            char[] tmpnumber = new char[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                if (alphabet.Contains(input[i]))
                    tmpalphabet[i] = input[i];
                else
                    tmpnumber[i] = input[i];
            }
            for (int i = 1; i < tmpalphabet.Length; i++)
            {
                for (int j = i; j > 0; j--)
                {
                    if (tmpalphabet[j - 1] > tmpalphabet[j])
                    {
                        char tmp = tmpalphabet[j - 1];
                        tmpalphabet[j - 1] = tmpalphabet[j];
                        tmpalphabet[j] = tmp;
                    }
                }
            }
            Array1Dim(tmpalphabet);
            Array1Dim(tmpnumber);
        }
        public static char[] Sort2(char[] input)
        {
            string alphanumeric = "abcdefghijklmnopqrstuvwxyz0123456789";
            
            for(int i = 1; i < input.Length; i++)
            {
                for(int j = i; j > 0; j--)
                {
                    if (alphanumeric.IndexOf(input[j]) < alphanumeric.IndexOf(input[j-1]))
                    {
                        char tmp = input[j-1];
                        input[j-1] = input[j];
                        input[j] = tmp;
                    }
                }
            }
            return input;
        }   
        public static void Array1Dim(char[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]}");
            }
        }
    }
}

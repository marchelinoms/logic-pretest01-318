﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    public class Soal02
    {
        public Soal02()
        {
            Console.WriteLine("Masukkan String yang dicari : ");
            string input = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            Console.WriteLine("Masukkan String : ");
            string check = String.Concat(Console.ReadLine().ToLower().Where(a => !Char.IsWhiteSpace(a)));
            Console.WriteLine(CheckKataDalamKalimat(input, check));
        }
        public static string CheckKataDalamKalimat(string s, string s2)
        {
            string result = "";
            int index = 0;
            for (int i = 0; i < s2.Length; i++)
            {
                if (index < s.Length && s2[i] == s[index])
                    index++;
            }
            result = index == s.Length ? "YES" : "NO";
            return result;
        }

    }
}

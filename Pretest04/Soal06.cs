﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    public class Soal06
    {
        public Soal06()
        {
            Console.WriteLine("Waktu masuk (yyyy-MM-ddThh:mm:ss) : ");
            string[] dtMasuk = Console.ReadLine().Split('T');
            string[] dateMasuk = dtMasuk[0].Split('-');
            string[] timeMasuk = dtMasuk[1].Split(':');
            DateTime masuk = new DateTime(int.Parse(dateMasuk[0]), int.Parse(dateMasuk[1]), int.Parse(dateMasuk[2]), int.Parse(timeMasuk[0]), int.Parse(timeMasuk[1]), int.Parse(timeMasuk[2]));

            Console.WriteLine("Waktu keluar (yyyy-MM-ddThh:mm:ss) : ");
            string[] dtKeluar = Console.ReadLine().Split('T');
            string[] dateKeluar = dtKeluar[0].Split('-');
            string[] timeKeluar = dtKeluar[1].Split(':');
            DateTime keluar = new DateTime(int.Parse(dateKeluar[0]), int.Parse(dateKeluar[1]), int.Parse(dateKeluar[2]), int.Parse(timeKeluar[0]), int.Parse(timeKeluar[1]), int.Parse(timeKeluar[2]));

            NotaParkir(masuk, keluar);
        }
        public static void NotaParkir(DateTime masuk, DateTime keluar)
        {
            TimeSpan interval = keluar - masuk;
            int hour = Convert.ToInt16(Math.Ceiling(interval.TotalHours));
            int day = hour / 24;
            hour = hour - (day * 24);
            double total = day * 50000;
            for(int i = 1; i <= hour; i++)
            {
                if (i == 1)
                    total += 5000;
                else if (i > 1 && i < 11)
                    total += 3000;
                else if (i > 10 && i < 24)
                    total += 2000;
            }
            Console.WriteLine(total);
        }
    }
}

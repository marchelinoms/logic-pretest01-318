﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    public class Soal02
    {
        public Soal02()
        {
            //            Input:
            //          0321837
            //            Output:
            //              4
            Console.Write("Masukkan Barisan Angka  : ");
            char[] barisan = Console.ReadLine().ToCharArray();
            Console.WriteLine(Selisih(barisan));
        }
        public static int Selisih(char[] barisan)
        {
            int[] barisanAngka = Array.ConvertAll(barisan, c => (int)Char.GetNumericValue(c));
            int ganjil = 0, genap = 0;
            for(int i = 0; i < barisanAngka.Length; i++)
            {
                if (barisanAngka[i] % 2 == 0)
                    genap += barisanAngka[i];
                else
                    ganjil += barisanAngka[i];
            }

            return Math.Abs(ganjil-genap);
        }
    }
}

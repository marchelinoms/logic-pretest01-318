﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    public class Soal07
    {
        public Soal07()
        {
            Console.Write("Hari Kerja Amir : ");
            int x = int.Parse(Console.ReadLine());
            Console.Write("Hari Kerja Supri : ");
            int y = int.Parse(Console.ReadLine());
            Console.WriteLine("Tanggal Libur bersama (yyyy-mm-dd) : ");
            DateTime z = DateTime.Parse(Console.ReadLine());
            LiburBersama(x, y, z);
        }
        public static void LiburBersama(int x, int y, DateTime z)
        {
            
            DateTime tanggalSementara = z;
            DateTime tanggalLiburPerkiraan = z.AddDays(x * y);
            while (tanggalSementara <= tanggalLiburPerkiraan)
            {
                if (tanggalSementara.DayOfWeek != DayOfWeek.Sunday && tanggalSementara.DayOfWeek != DayOfWeek.Saturday)
                {  
                  tanggalSementara = tanggalSementara.AddDays(1);
                }
            }
            while (tanggalSementara.DayOfWeek == DayOfWeek.Saturday || tanggalSementara.DayOfWeek == DayOfWeek.Sunday)
            {
                tanggalSementara = tanggalSementara.AddDays(1);
            }

            Console.WriteLine($"Tanggal Libur Bersama Berikutnya : {tanggalSementara.Day} {tanggalSementara.ToString("MMMM")} {tanggalSementara.Year}");
        }
    }
}

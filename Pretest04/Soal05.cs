﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    public class Soal05
    {
        public Soal05()
        {
            Console.Write("Masukkan Jumlah Uang : ");
            int uang = int.Parse(Console.ReadLine()); Console.WriteLine("Masukkan List Harga Item 1 (pisah menggunakan koma) : ");
            int[] harga1 = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);
            Console.Write("Masukkan List Harga Item 2 (pisah menggunakan koma) : ");
            int[] harga2 = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukkan List Harga Item 3 (pisah menggunakan koma) : ");
            int[] harga3 = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.WriteLine(MaximumBuy(uang,harga1,harga2,harga3));
        }
        public static int MaximumBuy(int uang,int[] harga1, int[] harga2, int[] harga3)
        {
            int i = 0 ,j = 0 ,k = 0, totalMax = 0;
            for(i = 0; i < harga1.Length; i++)
            {
                for( j = 0; j < harga2.Length; j++)
                {
                    for(k = 0; k < harga3.Length; k++)
                    {
                        int total = harga1[i] + harga2[j] + harga3        [k];
                        if(total <= uang && total >= totalMax)
                            totalMax = total;
                    }
                }
            }

            return totalMax;
        }
    }
}

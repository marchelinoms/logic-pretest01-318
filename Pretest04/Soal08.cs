﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    public class Soal08
    {
        public Soal08()
        {
            Console.Write("Masukkan input : ");
            int input = int.Parse(Console.ReadLine());
            Console.WriteLine(Fibonacci(input));
        }
        public static int Fibonacci(int input)
        {
            int[] n = new int[input];
            for(int i = 0; i < input; ++i)
            {
                if(i <= 1)
                    n[i] = 1;
                else
                    n[i] = n[i-1] + n[i-2];
            }

            return n[input-1];
        }
    }
}

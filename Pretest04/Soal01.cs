﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    public class Soal01
    {
        public Soal01()
        {
            //            Input 1:
            //              3
            //              2
            //          129762982
            //          Output 1:
            //          982129762
            Console.Write("Masukkan group angka : ");
            int group = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jumlah putaran : ");
            int putaran = int.Parse(Console.ReadLine());
            Console.Write("Masukkan barisan angka : ");
            string input = Console.ReadLine();
            Console.WriteLine(rotasi(input,group,putaran));
        }
        public string rotasi(string input,int group,int putaran)
        {
            return input.Substring(putaran * group) + input.Substring(0, putaran * group);
        }
    }
}

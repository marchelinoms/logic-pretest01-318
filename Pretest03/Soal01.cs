﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest03
{
    public class Soal01
    {
        public Soal01()
        {
            Console.WriteLine("Masukkan jumlah uang : ");
            int uang = int.Parse(Console.ReadLine());
            int stik = uang / 100;
            int hadiah = 0;

            while (stik >= 6)
            {
                hadiah += 1;
                stik -= 6;
            }
            Console.WriteLine($"Hadiah : {hadiah}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest03
{
    public class Soal02
    {
        public Soal02()
        {
            Console.Write("Masukkan string : ");
            string input = Console.ReadLine().ToLower();
            
            string lib = "abcdefghijklmnopqrstuvwxuz";
            int[] nilaiHuruf = new int[lib.Length];

            for(int i = 0; i < lib.Length; i++)
            {
                nilaiHuruf[i] = i + 1;
            }
            List<int> vokal = new List<int>();
            List<int> konsonan = new List<int>();
            for(int i = 0; i < input.Length; i++)
            {
                if (nilaiHuruf.Contains(input[i]))
                {
                    if (input[i] == 'a' || input[i] == 'i' || input[i] == 'u' || input[i] == 'e' || input[i] == 'o')
                    {
                        vokal.Add(nilaiHuruf[input[i]]);
                    }
                    else
                        konsonan.Add(nilaiHuruf[input[i]]);
                }
            }

            Console.WriteLine($"Selisih vokal dan konsonan {Math.Abs(vokal.Sum() - konsonan.Sum())} ");
        }
    }
}

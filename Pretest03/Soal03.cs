﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest03
{
    public class Soal03
    {
        public Soal03()
        {
            Console.WriteLine("Masukkan input : ");
            int[] input = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            
            foreach (int i in NaikTurunGunung(input))
            {
                Console.Write(i + " ");
            }
        }
        public int[] NaikTurunGunung(int[] input)
        {
            int[] result = new int[2];
            int naik = 0, turun = 0;
            for(int i = 0; i < input.Length;i++)
            {
                if(i < input.Length - 1)
                {
                    if (input[i] - input[i + 1] < 0)
                        naik++;
                    else
                        turun++;
                }
            }
            result[0] = naik;
            result[1] = turun;
            return result;
        }
    }
}
